This is an experiment in *not* using a Patricia trie for IP network prefixes. As usually implemented, tries are pointer-heavy structures. Pointer chasing, unless it hits cache, is one of the worst things you can do to a modern computer.

The application is a command-line tool that merges network address/prefix lists or computes most-specific coverings. These are batch operations that load data, compute a result, and dump the results.
This approach is not suitable if you want to compute on-line with varying prefixes and addresses, as would be the case in a router.

The structure we use instead of a trie is a simple sorted list in trie pre-order.
Sorted lists are easily merged. To compute covers, we keep a `stick` of nested coverings, most- to least-specific, which we adjust incrementally. This effectively generates the trie branches on demand.

The sorted list replaces trie traversal and pointer chasing with a linear scan, with much better memory locality. Of course, in python, our lists are lists of pointers anyway, so some of this is lost. A better solution is to write this in a language like Go, which supports memory structuring.

If the input data is already sorted, then it can be streamed. This requires only a small amount of working memory for current items and a covering `stick`. The internal prefix sort can be replaced by a multi-pass external sort, making it possible to process sorted data much larger than fits in memory.


# DATA

Input and output consists of text files.
Each line has an IPv4 or IPv6 address or CIDR prefix key and an optional string tag.
An address is equivalent to a full-length CIDR prefix.
Only the last line with a duplicate key is used.
A line beginning with "#" is logged and ignored.

Data can mix IPv4 and IPv6. By convention, IPv6 sorts after IPv4.

Output is always in trie order, so it can be used as streaming input.

Input and output item counts are printed to stderr.


# OPERATIONS

Operations are

*   `join` set union
*   `meet` set intersection
*   `drop` asymmetric set difference
*   `incl` most-specific covering prefix
*   `excl` uncovered

`join`, `meet`, and `drop` don't consider prefix covering, so an address and its covers are considered distinct. A prefix `join` or `meet` to itself keeps the latter tag.

`incl` and `excl` consider prefix covering. `incl` uses the tag from the prefix. If there is no tag, it uses the covering prefix as a tag.

Operations are applied left-to-right across arguments.


# USAGE

    usage: pastrie.py [-h] [-x | -i] [-c] [--join | --meet | --drop | --incl | --excl] [data [data ...]]

    compute with sets of IP prefixes and attributes

    positional arguments:
      data        prefix data files or - for stdin

    optional arguments:
      -h, --help  show this help message and exit
      -x          externally sorted data
      -i          internally sorted data
      -c          just count, no output data
      --join      merge lists
      --meet      intersect lists
      --drop      subtract lists
      --incl      most-specific covers
      --excl      not covered


# EXAMPLES

The default operation is `join`, with an internal sort, so the following example sorts and merges by address.

    pastrie.py targets*

Count addresses common to all target lists.

    pastrie.py --meet -c targets*

Filter out addresses in RFC prefixes.

    pastrie.py --excl targets RFC*

Classify each address in a sorted list by most-specific routed covering, after sorting the routing table. "-" designates stdin.

    pastrie.py routed | pastrie.py -x --incl addrs -

Classify each address in a sorted list by country code, where `cc` is a sorted list of prefixes with country codes.

    pastrie.py -x --incl addrs cc

Count additions and deletions to routing tables.

    pastrie.py --drop -c routing-1 routing-0
    pastrie.py --drop -c routing-0 routing-1

Find addresses in `100.64.0.0/10`.

    pastrie.py --incl -x addrs <(echo 100.64.0.0/10)


# EXTRAS

There are two other utilities here.

*   `join.py` joins two sorted address lists, concatenating address attributes.
    It can do inner and outer joins.

*   `a6.py` is a utility for sorting IPv6 addresses with `sort -V`.
    It encodes IPv6 addresses into dotted decimal so that they sort correctly or decodes the result back to addresses.
    IPv4 addresses are unchanged.
    Use it like

        ... | a6.py --encode | sort -V | a6.py --decode | pastrie.py -x ...

