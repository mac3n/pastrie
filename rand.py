"""
    generate random prefix sets
"""

import random


def width(bits):
    "choose prefix width"
    # nodes in trie
    while bits:
        # leaf nodes
        a = 1 << bits
        # all nodes
        t = (a << 1) - 1
        if random.randrange(t) < a:
            break
        bits -= 1
    return bits

def prefix(width):
    "Make a random prefix"
    a = (random.randrange(1 << width) << (32 - width))
    n = [(b & 0xFF) for b in (a >> 24, a >> 16, a >> 8, a)]
    return ".".join(str(b) for b in n) + "/%d" % width

    
if __name__ == "__main__":
    import argparse
    p = argparse.ArgumentParser(description=__doc__)
    p.add_argument("--bits", type=int, default=32, help="address bits")
    p.add_argument("--count", type=int, default=1, help="prefix count")
    args = p.parse_args()

    for i in range(args.count):
        print(prefix(width(args.bits)), i, sep="\t")
