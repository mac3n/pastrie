#!/usr/bin/python3
"""
    merge sorted lists of IP prefixes and attributes
"""
import sys
import pastrie


if __name__ == "__main__":
    import argparse

    p = argparse.ArgumentParser(description=__doc__)
    x = p.add_mutually_exclusive_group()
    x.add_argument("-1", dest="only1", action="store_true", help="data from 1 only")
    x.add_argument("-2", dest="only2", action="store_true", help="data from 2 only")
    x.add_argument("-12", dest="both", action="store_true", help="data from 1 and 2 only")
    p.add_argument("-j", "--join", metavar="delimiter", default="\t", help="join attributes with delimiter")
    p.add_argument("-e", "--empty", metavar="empty", type=str, default="--", help="default for missing attributes (--)")
    p.add_argument("seq", metavar="file", nargs=2, help="address-sorted attributes")
    args = p.parse_args()

    (s1, s2) = (pastrie.Stream(pastrie.load(sys.stdin if a == "-" else open(a))) for a in args.seq)
    n = nl = nr = 0
    for ((lk, la), (rk, ra)) in pastrie.match(s1.items(), s2.items()):
        n += 1
        if args.both:
            if rk is None:
                nl += 1
            elif lk is None:
                nr += 1
            else:
                print(lk, "\t", la or args.empty, args.join, ra or args.empty, sep="")
        elif args.only1:
            if lk is None:
                nr += 1
            else:
                print(lk, "\t", la or args.empty, args.join, ra or args.empty, sep="")
        elif args.only2:
            if rk is None:
                nl += 1
            else:
                print(rk, "\t", la or args.empty, args.join, ra or args.empty, sep="")
        else:
            if rk is None:
                nl += 1
            elif lk is None:
                nr += 1
            print(lk or rk, "\t", la or args.empty, args.join, ra or args.empty, sep="")

    print("1-2: %d" % nl, "2-1: %d" % nr, "1+2: %d" % (n - nl - nr), sep="\t", file=sys.stderr)
