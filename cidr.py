
from socket import inet_pton, inet_ntop, AF_INET, AF_INET6

# bitbanging in bytearrays
# bits are big-endian, 0..N-1


def btrim(addr, bits):
    "Clear trailing bytearray bits"
    if not (0 <= bits <= len(addr) * 8):
        raise KeyError("bad prefix width", bits)
    i, r = divmod(bits, 8)
    if i < len(addr):
        if r:
            # snuff partial byte
            addr[i] &= 0xFF00 >> r
            i += 1
            # trailing 0s
        addr[i:] = bytearray(len(addr) - i)


def test_btrim():
    "Test clear trailing bits"
    for bits in range(33):
        addr = bytearray([0xFF] * 4)
        btrim(addr, bits)
        n = (((((addr[0] << 8) + addr[1]) << 8) + addr[2]) << 8) + addr[3]
        assert n == 0xFFFFFFFF & (-1 << (32 - bits))


def bincr(addr, bit):
    "Increment some addr bit."
    # increment point
    if not (0 <= bit < len(addr) * 8):
        raise KeyError("bad prefix width", bit)
    i, r = divmod(bit, 8)
    # carry in
    c = 0x80 >> r
    # propagate add with carry
    while 0 <= i and c:
        s = addr[i] + c
        addr[i], c = s & 0xFF, s >> 8
        i -= 1


def test_bincr():
    "Test increment bit"
    for bit in range(0, 32):
        addr = bytearray([0] * 4)
        bincr(addr, bit)
        n = (((((addr[0] << 8) + addr[1]) << 8) + addr[2]) << 8) + addr[3]
        assert n == 0x80000000 >> bit
        addr = bytearray([0xFF] * 4)
        bincr(addr, bit)
        n = (((((addr[0] << 8) + addr[1]) << 8) + addr[2]) << 8) + addr[3]
        assert n == 0x7FFFFFFF >> bit


def bdiff(a, b, bits):
    "Find first difference in bytes"
    # first difference in bytes
    for i in range(0, -(-bits // 8)):
        x = a[i] ^ b[i]
        if x:
            break
    else:
        # no diffs in range
        return None
    # find first bit in difference
    bits = 8 * i
    if not x & 0xF0:
        bits += 4
        x <<= 4
    if not x & 0xC0:
        bits += 2
        x <<= 2
    if not x & 0x80:
        bits += 1
        x <<= 1
    assert x & 0x80
    return bits


def test_bdiff():
    "Test find first difference"
    a = bytearray(4)
    for i in range(32):
        bit = bdiff(a, a, 32)
        assert bit is None
        b = bytearray(a)
        bincr(b, i)
        bit = bdiff(a, b, i + 1)
        assert bit == i


def atob(pfx):
    "Convert string to addr bytearray and length."
    pfx = pfx.rsplit("/", 1)
    # convert addr octets
    p = pfx[0]
    af, bits = AF_INET, 32
    if ":" in p:
        af, bits = AF_INET6, 128
    addr = bytearray(inet_pton(af, p))
    # prefix length
    if 1 < len(pfx):
        bits = int(pfx[1])
        btrim(addr, bits)
    return (addr, bits)


def test_atob():
    "Test prefix string conversion"
    for i in range(33):
        (addr, bits) = atob("0.0.0.0/%d" % i)
        n = (((((addr[0] << 8) + addr[1]) << 8) + addr[2]) << 8) + addr[3]
        assert n == 0 and bits == i
    for i in range(32):
        (addr, bits) = atob("1.2.3.4/%d" % i)
        assert bits == i
        n = (((((addr[0] << 8) + addr[1]) << 8) + addr[2]) << 8) + addr[3]
        assert n == 0x01020304 >> (32 - i) << (32 - i)
    (addr, bits) = atob("::")
    assert addr == bytearray(16)
    assert bits == 128
    (addr, bits) = atob("2a01:c50f::fe24:afc0")
    assert addr[:4] == bytearray([0x2a, 0x01, 0xc5, 0x0f])
    assert addr[4:12] == bytearray(8)
    assert addr[12:] == bytearray([0xfe, 0x24, 0xaf, 0xc0])
    assert bits == 128


# represent a prefix as address length followed by address bytes followed by prefix width
# this gives the correct trie sort order using builtin compare
# provided we clear extra host bits

# we use immutable bytes() instead of bytearray() to avoid another indirection
# this means that we need to construct in __new__ instead of __init__

class Net(bytes):
    "CIDR prefix as bytes with bitwidth"

    # just the bare thing, no attributes
    __slots__ = ()

    def __new__(cls, pfx=None, addr=None, bits=None):
        "initialize immutable from prefix string or binary parts"
        if pfx is not None:
            (addr, bits) = atob(pfx)
        elif bits is None:
            bits = len(addr) * 8
        else:
            btrim(addr, bits)
        # insert length and append bits
        addr[0:0] = [len(addr)]
        addr.append(bits)
        return bytes.__new__(cls, addr)

    # prefix arithmetic

    def trim(self, bits):
        "Changed bitwidth"
        addr = bytearray(self[1:-1])
        return Net(addr=addr, bits=bits)

    def incr(self, bit):
        "Incremented addr."
        addr, bits = bytearray(self[1:-1]), self[-1]
        # increment point
        if not (bit < bits):
            raise KeyError("bad prefix width", bit)
        bincr(addr, bit)
        return Net(addr=addr, bits=bits)

    # attributes

    def af(self):
        "Address format"
        return AF_INET if self[0] == 4 else AF_INET6

    def net(self):
        "Network address"
        return inet_ntop(self.af(), self[1:-1])

    def width(self):
        "Network prefix width"
        return self[-1]

    def __str__(self):
        "Convert to CIDR notation"
        p, width = self.net(), self[-1]
        if width != self[0] * 8:
            # add bit count
            p = "%s/%d" % (p, width)
        return p

    def __repr__(self):
        return "Net('%s')" % str(self)

    # this representation gives correct trie order
    # with ipv4 sorting before ipv6

    def __contains__(self, them):
        "Prefix containment"
        if len(self) != len(them):
            # no 4-in-6
            return False
        bits = bdiff(self[1:], them[1:], min(self.width(), them.width()))
        if bits is None:
            # same network address
            return self.width() <= them.width()
        # enough bits match
        return self.width() <= bits


def test_pfx4():
    "Test ipv4 encoding"
    p = Net("8.8.8.8")
    assert p.width() == 32
    assert str(p) == "8.8.8.8"
    p = Net("8.8.8.8/24")
    assert str(p) == "8.8.8.0/24"
    p = Net("8.8.15.0/21")
    assert str(p) == "8.8.8.0/21"


def test_pfx6():
    "Test ipv6 encoding"
    p = Net("2000:2000::1")
    assert str(p) == "2000:2000::1"
    p = Net("2000:2000::1/32")
    assert str(p) == "2000:2000::/32"
    p = Net("2000:ffff::1/28")
    assert str(p) == "2000:fff0::/28"


def test_order():
    "Test trie ordering"
    p0, p1 = Net("8.8.8.8"), Net("2000:2000::1")
    assert p0 < p1
    p0, p1 = Net("200.200.200.200"), Net("0808:0808::")
    assert p0 < p1
    p0, p1 = Net("32.0.64.0/19"), Net("2000:db8::/32")
    assert p0 < p1
    p0 = Net("85.170.85.0/24")
    for i in range(256):
        p1 = Net("85.170.%d.0/24" % i)
        if i < 85:
            assert p1 < p0
        elif 85 < i:
            assert p0 < p1
        else:
            assert p0 == p1
    p0 = Net("85.170.85.0/24")
    for i in range(32):
        p1 = Net("85.170.85.0/%d" % i)
        if i < 24:
            assert p1 < p0
        elif 24 < i:
            assert p0 < p1
        else:
            assert p0 == p1


def test_trim():
    "Test prefix length trim."
    p = Net("8.8.8.8")
    assert p.width() == 32
    assert p.net() == "8.8.8.8"
    q = p.trim(29)
    assert q.width() == 29
    assert q.net() == "8.8.8.8"
    q = p.trim(28)
    assert q.width() == 28
    assert q.net() == "8.8.8.0"
    q = p.trim(0)
    assert q.width() == 0
    assert q.net() == "0.0.0.0"


def test_incr():
    "Test prefix offset."
    p = Net("8.8.8.8")
    q = p.incr(23)
    assert q.net() == "8.8.9.8"
    p = Net("8.8.255.8")
    q = p.incr(23)
    assert q.net() == "8.9.0.8"
    q = Net("::")
    for i in range(128):
        q = q.incr(i)
    assert q == Net("FFFF:FFFF:FFFF:FFFF:FFFF:FFFF:FFFF:FFFF")
    q = q.incr(127)
    assert q == Net("::")


def test_in():
    "Test prefix containment"
    p0, p1 = Net("8.8.8.8"), Net("2000:2000::1")
    assert p0 not in p1 and p1 not in p0
    assert p0 in p0 and p1 in p1
    pi, pj = Net("85.170.85.170"), Net("85.170.85.170")
    for i in range(32):
        for j in range(32):
            pi, pj = Net("85.170.85.170/%d" % i), Net("85.170.85.170/%d" % j)
            if i < j:
                assert pj in pi
            elif j < i:
                assert pi in pj
            else:
                assert pj in pi
                assert pi in pj


if __name__ == "__main__":

    test_btrim()
    test_bincr()
    test_bdiff()
    test_atob()

    test_pfx4()
    test_pfx6()
    test_order()
    test_trim()
    test_incr()
    test_in()
