#!/usr/bin/env python3
"""
    encode/decode IPv6 addresses and prefixes so they sort with `sort -V`
"""
import sys
from socket import inet_pton, inet_ntop, AF_INET6


def encode(addr):
    "Encode a4/a6 for sort"
    if ":" not in addr:
        return addr
    a = inet_pton(AF_INET6, addr)
    w = [(a[i] << 8) + a[i + 1] for i in range(0, 16, 2)]
    return ".".join(str(i) for i in w)


def decode(addr):
    "Decode a4/a6 for sort"
    w = [int(i) for i in addr.split(".")]
    if len(w) == 4:
        return addr
    a = [w[i // 2] >> 8 if not (i & 1) else w[i // 2] & 255 for i in range(16)]
    return inet_ntop(AF_INET6, bytes(a))


if __name__ == "__main__":
    import argparse

    p = argparse.ArgumentParser(description=__doc__)
    x = p.add_mutually_exclusive_group(required=True)
    x.add_argument("-e", "--encode", action="store_true", help="encode addresses")
    x.add_argument("-d", "--decode", action="store_true", help="decode addresses")
    p.add_argument("addrs", metavar="addresslist", nargs="?", help="IP address list")
    args = p.parse_args()

    coding = encode if args.encode else decode
    for line in open(args.addrs) if args.addrs else sys.stdin:
        line = line.strip().split(None, 1)
        cidr = line[0].split("/", 1)
        print(coding(cidr[0]), end="")
        if 1 < len(cidr):
            print("/", cidr[1], sep="", end="")
        if 1 < len(line):
            print("\t", line[1], sep="")
        else:
            print()
