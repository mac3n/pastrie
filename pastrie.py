#!/usr/bin/python3
"""
    compute with sets of IP prefixes and attributes
"""
from __future__ import print_function

import cidr
import sys


# we pack an entire table entry into an immutable bytestring
# this lets us get away with fewer pointer dereferences
# immutable means that there doesn't need to be an extra pointer to the bytes

class Pack(bytes):
    "Packed CIDR key, text tag"

    __slots__ = ()

    def __new__(cls, pfx, tag):
        "Packed bytes items"
        if tag is None:
            return bytes.__new__(cls, pfx)
        return bytes.__new__(cls, pfx + bytes(tag, "utf8"))

    def key(self):
        "sort key bytes"
        # addr len + addr + prefix width
        return self[:self[0] + 2]

    def pair(self):
        "Key, tag pair"
        key = self[:self[0] + 2]
        pfx = cidr.Net(addr=bytearray(key[1:-1]), bits=key[-1])
        return (pfx, str(self[len(key):], "utf8") if len(key) < len(self) else None)


def test_pack():
    "Test item pack."
    it0, it1 = Pack(cidr.Net("8.8.8.0/24"), None), Pack(cidr.Net("8.8.8.0/24"), "goog")
    assert it0.pair() == (cidr.Net("8.8.8.0/24"), None)
    assert it1.pair() == (cidr.Net("8.8.8.0/24"), "goog")


def sortu(tbl):
    "de-duplicate after sort, squashing list."
    # sort by prefix only
    pfx0, uniq = None, 0
    tbl.sort(key=Pack.key)
    # squash duplicate keys
    for (i, it) in enumerate(tbl):
        pfx1 = it.key()
        if pfx1 == pfx0:
            # overwrite
            tbl[uniq - 1] = it
        else:
            # slide down
            if uniq <= i:
                tbl[uniq] = it
            pfx0 = pfx1
            uniq += 1
    # trim
    del tbl[uniq:]
    return tbl


def test_dedup():
    "Test dedups."
    tbl = [Pack(b"\x01\x01\x08", "2"), Pack(b"\x01\x02\x08", "-2"), Pack(b"\x01\x00\x08", "0"), Pack(b"\x01\x01\x08", "1")]
    t = sortu(tbl)
    assert t == [Pack(b"\x01\x00\x08", "0"), Pack(b"\x01\x01\x08", "1"), Pack(b"\x01\x02\x08", "-2")]


class Table(object):
    "Sorted prefix, tag items."

    __slots__ = ("tbl", "len", "dup")

    def __init__(self, items):
        self.tbl = [Pack(*it) for it in items]
        len0 = len(self.tbl)
        sortu(self.tbl)
        self.len = len(self.tbl)
        self.dup = len0 - self.len

    def items(self):
        "yield prefix-ordered items."
        for it in self.tbl:
            yield it.pair()


def test_table():
    "Test table order."
    tbl = Table(items=[(cidr.Net("2000::%x" % (i ^ 1)), str(i)) for i in range(6)])
    pfxs = [p for (p, _) in tbl.items()]
    assert pfxs == [cidr.Net("2000::%x" % i) for i in range(6)]
    tags = [t for (p, t) in tbl.items()]
    assert tags == [str(i ^ 1) for i in range(6)]


def test_uniq():
    "Test table uniqueness."
    tbl = Table(items=[(cidr.Net("2000::b020"), str(i)) for i in range(6)])
    pfxs = [p for (p, _) in tbl.items()]
    assert pfxs == [cidr.Net("2000::b020")]
    tags = [t for (p, t) in tbl.items()]
    assert tags == [str(6 - 1)]


# i/o

def load(src):
    "load from file"
    for line in src:
        line = line.strip()
        if not line:
            continue
        if line.startswith("#"):
            print(line, file=sys.stderr)
            continue
        line = line.split(None, 1)
        pfx = cidr.Net(line[0])
        yield (pfx, line[1] if 1 < len(line) else None)


def dump(items):
    "Print prefixes with optional value."
    n = 0
    for (pfx, tag) in items:
        n += 1
        if tag is None:
            print(pfx)
        else:
            print(pfx, tag, sep="\t")
    return n


def drain(items):
    "Discard result."
    n = 0
    for item in items:
        n += 1
    return n


class Stream(object):
    "Ordered prefix stream."

    __slots__ = ("stream", "len", "dup")

    def __init__(self, items):
        self.stream = items
        self.len = self.dup = 0

    def items(self):
        "Yield ordered items, with LAST duplicate pfx."
        last = None
        for it in self.stream:
            (pfx, _) = it
            if last is None:
                pass
            elif last[0] < pfx:
                # new pfx
                yield last
                self.len += 1
            elif last[0] == pfx:
                # duplicated
                self.dup += 1
            else:
                raise KeyError("%s before %s" % (last[0], pfx), self.len + self.dup)
            # dalay in case of duplicate
            last = it
        # final delayed item
        if last is not None:
            yield last
            self.len += 1


def test_stream():
    "Test stream order & dedup."
    s = Stream((cidr.Net("192.0.%d.0" % i), i) for i in range(64))
    for it in s.items():
        pass
    assert s.len == 64 and s.dup == 0
    s = Stream((cidr.Net("192.0.11.0"), i) for i in range(64))
    for it in s.items():
        pass
    assert s.len == 1 and s.dup == 63
    assert it == (cidr.Net("192.0.11.0"), 63)
    s = Stream((cidr.Net("192.0.%d.0" % (i ^ 2)), i) for i in range(64))
    try:
        for it in s.items():
            pass
    except KeyError:
        # expect error
        pass
    else:
        assert False


# end of (pfx, tag) items
EOI = (None, None)


def match(lhs, rhs):
    "Collate two sequences."
    (lk, _) = lit = next(lhs, EOI)
    (rk, _) = rit = next(rhs, EOI)
    # inorder
    while lk is not None and rk is not None:
        if lk < rk:
            yield (lit, EOI)
            (lk, _) = lit = next(lhs, EOI)
        elif rk < lk:
            yield (EOI, rit)
            (rk, _) = rit = next(rhs, EOI)
        else:
            yield (lit, rit)
            (lk, _) = lit = next(lhs, EOI)
            (rk, _) = rit = next(rhs, EOI)
    # trailers
    while lk is not None:
        yield (lit, EOI)
        (lk, _) = lit = next(lhs, EOI)
    while rk is not None:
        yield (EOI, rit)
        (rk, _) = rit = next(rhs, EOI)


def test_match():
    "Test collated match."
    a = [(cidr.Net("2000::%x" % i), str(i)) for i in range(0, 13, 2)]
    b = [(cidr.Net("2000::%x" % i), str(i)) for i in range(0, 13, 3)]
    c = []
    for (lit, rit) in match(Table(a).items(), Table(b).items()):
        if lit is not EOI and rit is not EOI:
            (_, tag) = rit
            c.append(tag)
    assert c == [str(i) for i in (0, 6, 12)]
    # trailing items
    c = 0
    for (lit, rit) in match(Table(a).items(), iter([])):
        c += 1
        assert rit is EOI
    assert c == len(a)
    c = 0
    for (lit, rit) in match(iter([]), Table(b).items()):
        c += 1
        assert lit is EOI
    assert c == len(b)


# streaming combining operations


class Stick(object):
    "Incremental trie branch as a stack."

    __slots__ = ("stack")

    def __init__(self, items=None):
        self.stack = list(items) if items else []
        self.stack.sort()

    def __str__(self):
        return ",".join("%s=%s" % it for it in reversed(self.stack))

    def __repr__(self):
        return "Stick([%s])" % self if self.stack else "Stick()"

    def __len__(self):
        return len(self.stack)

    def tip(self):
        "most-specific covering."
        return self.stack[-1] if self.stack else EOI

    def walk(self, pfx):
        "walk branch to a prefix"
        while self.stack:
            (tip, _) = self.stack[-1]
            if pfx in tip:
                # on branch
                break
            # pop off earlier branch
            self.stack.pop()
        # at this point the stack is less-specifics of pfx

    def push(self, item):
        "extend branch"
        self.stack.append(item)


def test_stick():
    "Test prefix sticks."
    s = Stick()
    # push prefixes
    t0 = cidr.Net("2000::/3")
    s.push((t0, 0))
    assert s.tip() == (t0, 0)
    t1 = cidr.Net("2000:b020::/32")
    s.walk(t1)
    assert s.tip() == (t0, 0)
    s.push((t1, 1))
    assert s.tip() == (t1, 1)
    t2 = cidr.Net("2000:b020::b17")
    s.walk(t2)
    assert s.tip() == (t1, 1)
    s.push((t2, 2))
    assert s.tip() == (t2, 2)
    # pop
    t3 = cidr.Net("2000:b020::bad")
    s.walk(t3)
    assert s.tip() == (t1, 1)
    t4 = cidr.Net("ffff::/32")
    s.walk(t4)
    assert s.tip() == EOI


def cover(more, less):
    "Find most-specific coverings"
    stick = Stick()
    (mk, _) = mit = next(more, EOI)
    (lk, _) = lit = next(less, EOI)
    # build trie path
    while lit is not EOI and mit is not EOI:
        # walk up to this point
        if mk < lk:
            stick.walk(mk)
            yield (mit, stick.tip())
            (mk, _) = mit = next(more, EOI)
        else:
            stick.walk(lk)
            stick.push(lit)
            (lk, _) = lit = next(less, EOI)
    while mit is not EOI:
        stick.walk(mk)
        yield (mit, stick.tip())
        (mk, _) = mit = next(more, EOI)


def test_cover():
    "Test collated coverings."
    a = [(cidr.Net("2000:%x::" % i), str(i)) for i in range(0, 16)]
    b = [(cidr.Net("2000:%x::/30" % i), str(i)) for i in range(0, 24, 8)]
    c = []
    for (mit, lit) in cover(Table(a).items(), Table(b).items()):
        if lit is not EOI:
            (_, tag) = lit
            c.append(tag)
    assert c == [str(i) for i in (0, 0, 0, 0, 8, 8, 8, 8)]
    # equality case
    a = [(cidr.Net("9.9.9.9"), None)]
    c = []
    for (mit, lit) in cover(Table(a).items(), Table(a).items()):
        if lit is not EOI:
            c.append(lit)
    assert len(c) == 1


# Streaming operations

def join(lhs, rhs):
    "Union"
    for (lit, rit) in match(lhs, rhs):
        if rit is not EOI:
            yield rit
        elif lit is not EOI:
            yield lit
        else:
            yield EOI


def meet(lhs, rhs):
    "Intersection"
    for (lit, rit) in match(lhs, rhs):
        if rit is not EOI and lit is not EOI:
            yield rit


def drop(lhs, rhs):
    "Asymmetric difference"
    for (lit, rit) in match(lhs, rhs):
        if rit is not EOI:
            continue
        elif lit is not EOI:
            yield lit


def incl(lhs, rhs):
    "Translate"
    for (lit, rit) in cover(lhs, rhs):
        if rit is not EOI:
            (lk, _), (rk, rv) = lit, rit
            yield (lk, rv if rv is not None else str(rk))


def excl(lhs, rhs):
    "Untranslated"
    for (lit, rit) in cover(lhs, rhs):
        if rit is EOI:
            yield lit


if __name__ == "__main__":
    import argparse

    p = argparse.ArgumentParser(description=__doc__)
    x = p.add_mutually_exclusive_group()
    x.add_argument("-i", dest="sort", const=Table, action="store_const", default=Table, help="internally sorted data")
    x.add_argument("-x", dest="sort", const=Stream, action="store_const", help="externally sorted data")
    q = p.add_mutually_exclusive_group()
    q.add_argument("--join", dest="op", const=join, action="store_const", default=join, help="merge lists")
    q.add_argument("--meet", dest="op", const=meet, action="store_const", help="intersect lists")
    q.add_argument("--drop", dest="op", const=drop, action="store_const", help="subtract lists")
    q.add_argument("--incl", dest="op", const=incl, action="store_const", help="most-specific covers")
    q.add_argument("--excl", dest="op", const=excl, action="store_const", help="not covered")
    p.add_argument("data", nargs="+", help="prefix data files or - for stdin")
    p.add_argument("-c", action="store_true", help="just count, no output data")
    p.add_argument("-m", metavar="TEXT", action="append", help="include # message")
    args = p.parse_args()

    # build accumulate pipeline
    # drop, incl, excl are not associative
    srcs, acc = [], None
    for a in args.data:
        f = load(sys.stdin if a == "-" else open(a))
        s = args.sort(f)
        srcs.append((a, s))
        if acc is None:
            acc = s.items()
        else:
            acc = args.op(acc, s.items())
    # inject messages
    if args.m:
        for m in args.m:
            print("#", m)
    # result
    if acc is not None:
        if args.c:
            n = drain(acc)
        else:
            n = dump(acc)
        # counts
        for (a, s) in srcs:
            if s.dup:
                print(s.len, "/", s.len + s.dup, end="\t", file=sys.stderr)
            else:
                print(s.len, end="\t", file=sys.stderr)
            print(a, file=sys.stderr)
        print(n, ">", sep="\t", file=sys.stderr)
